Before:

![Before](https://gitlab.com/FaySmash/Material-Design-for-Nextcloud/raw/master/preview_before.jpg)

After:

![After](https://gitlab.com/FaySmash/Material-Design-for-Nextcloud/raw/master/preview_after.jpg)

**Installation**:

To use this Theme you have to have the **[Custom CSS](https://apps.nextcloud.com/apps/theming_customcss)** App for Nextcloud installed. Then login into your Nextcloud as Admin and navigate to /index.php/settings/admin/theming (Settings -> Theming) and Copy&Paste the Content of the Material-Design-for-Nextcloud.css into the Custom CSS Field at the Bottom.
Just hit Save and you're done!

**Configuration**:

At the moment, the Color is hard-coded. To change it according to your Theme Color, just do a search and replace for `0, 130, 201` and `0,130,201` and replace them with the RGB Values of your current Theme Color. If you're using a bright theme, also do a search and replace for `rgb(255,255,255)` and replace it with `rgb(0,0,0)` (or a different dark Color).
Also, the Ordner of the Icons in the Header at the Top are fixed, use the App **[AppOrder](https://apps.nextcloud.com/apps/apporder)** to arrange them in following Order:

**Files -> Gallery -> Music -> Calendar -> Contacts -> [other Apps]**

Furthermore you have to have the **External Sites** app installed.
If you don't have all of those Apps installed, it will cause problems too.

Edit the CSS as following if you don't have the **External Sites** App Apps:

You have to comment out the Lines below this comment:  **/\* replaces the External Sites Icon (Extension) \*/**  and afterwards you have to decrease the **li:nth-child( )** Value of the following Lines **by one**.

Edit the CSS as following if your Header contains different Apps:

Search for this Line:

---

```    /* ---------------------------------------------------------------------- TOP BAR BUTTONS ---------------------------------------------------------------------- */```

---

below it you'll find Code similar to this:


```
    /* removes all default Icons */
    #appmenu > li:nth-child(-n+5) > a > svg > image {
        display: none;
    }

    /* replaces the Files Icon */
    #appmenu > li:nth-child(1) > a:nth-child(1) > svg:nth-child(1) {
        background-image: var(--files-icon);
        background-size: calc(var(--top-bar-icon-size) + 2px) auto;
    }
```

This Line declares, how many Icon starting from left in the Header should be hidden in favor to be replaced (default 5):

`    #appmenu > li:nth-child(-n+ ` >THE AMOUNT OF ICONS (FROM LEFT, STARTING FROM 1) WHICH SHOULD BE HIDDEN IN THE HEADER GO HERE< ` ) > a > svg > image`

This line declares, which App Icon in the Header gets replaced:

`    #appmenu > li:nth-child(` >THE POSITION NUMBER (FROM LEFT, STARTING FROM 1) OF THE APP ICON IN THE HEADER GOES HERE< `) > a:nth-child(1) > svg:nth-child(1)`

Alter this Line according to your Setup, delete or comment out the Declerations for Apps you haven't installed or change the Position Number if you have the App installed but on a different Position in the Header.

---

**THIS PROJECT IS STILL WiP, PLEASE CHECK FOR NEW VERSIONS FREQUENTLY. BUGS ARE TOO BE EXPECTED (BUT NO CRITICAL ONES)**

If you would like to contribute, just request access.